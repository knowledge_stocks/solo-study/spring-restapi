package io.gitlab.kakaruu.practice.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ArticleVO {
  private int articleNO;
  private String writer;
  private String title;
  private String content;
}
