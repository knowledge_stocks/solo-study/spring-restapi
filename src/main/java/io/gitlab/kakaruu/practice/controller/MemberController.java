package io.gitlab.kakaruu.practice.controller;

import io.gitlab.kakaruu.practice.vo.MemberVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*RestController로 설정하면 모든 함수가 기본적으로 RequestBody로 설정된다.*/
@RestController
public class MemberController {
  Logger logger = LoggerFactory.getLogger(MemberController.class);

  // 그냥 문자열을 반환하면 Content-Type: text/html 로 반환된다.
  @RequestMapping(value = "/hello", method = RequestMethod.GET)
  public String hello() {
    logger.debug("GET /hello");

    return "Hello REST!!";
  }

  // object를 반환하면 json 형식으로 반환된다.(Content-Type: text/json)
  @RequestMapping(value = "/member", method = RequestMethod.GET)
  public MemberVO member() {
    logger.debug("GET /member");

    MemberVO vo = new MemberVO();
    vo.setId("hong");
    vo.setPwd("1234");
    vo.setName("홍길동");
    vo.setEmail("hong@test.com");
    return vo;
  }

  @RequestMapping(value = "/member/list", method = RequestMethod.GET)
  public List<MemberVO> memberList () {
    logger.debug("GET /member/list");

    List<MemberVO> list = new ArrayList<MemberVO>();
    for (int i = 0; i < 10; i++) {
      MemberVO vo = new MemberVO();
      vo.setId("hong"+i);
      vo.setPwd("123"+i);
      vo.setName("홍길동"+i);
      vo.setEmail("hong"+i+"@test.com");
      list.add(vo);
    }
    return list;
  }

  @RequestMapping(value = "/member/map", method = RequestMethod.GET)
  public Map<Integer, MemberVO> memberMap() {
    logger.debug("GET /member/map");

    Map<Integer, MemberVO> map = new HashMap<Integer, MemberVO>();
    for (int i = 0; i < 10; i++) {
      MemberVO vo = new MemberVO();
      vo.setId("hong" + i);
      vo.setPwd("123"+i);
      vo.setName("홍길동" + i);
      vo.setEmail("hong"+i+"@test.com");
      map.put(i, vo);
    }
    return map;
  }

  /*다른 매핑 주소와 겹치지 않는 path만 이쪽으로 매핑되는 방식인 것 같다.*/
  @RequestMapping(value= "/member/{num}", method = RequestMethod.GET)
  public int member(@PathVariable("num") int num ) throws Exception {
    logger.debug("GET /member/{num}");
    return num;
  }

  @RequestMapping(value= "/member", method = RequestMethod.POST)
  /*
  @RequestBody로 설정하면 body에 application/json 형식으로 요청해야 한다.
  @RequestParam은 URL의 쿼리 스트링으로 들어오는 값을 의미하므로 POST에서는 굳이 사용하지 말자.
  */
  public String member(@RequestBody MemberVO vo) throws Exception {
    logger.debug("POST /member");

    // POST는 주로 insert 작업을 의미

    return vo.toString();
  }

  @RequestMapping(value = "/member/list2", method = RequestMethod.GET)
  /*
  ResponseEntity를 통해 실행 결과 코드를 함께 리턴하는 방법
  */
  public ResponseEntity<List<MemberVO>> memberList2() {
    logger.debug("GET /member/list2");

    List<MemberVO> list = new ArrayList<MemberVO>();
    for (int i = 0; i < 10; i++) {
      MemberVO vo = new MemberVO();
      vo.setId("lee" + i);
      vo.setPwd("123"+i);
      vo.setName("이순신" + i);
      vo.setEmail("lee"+i+"@test.com");
      list.add(vo);
    }
    return new ResponseEntity(list, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @RequestMapping(value = "/member2", method = RequestMethod.GET)
  /*
  script를 통해 리다이렉트하는 방법
  */
  public ResponseEntity member2() {
    logger.debug("GET /member2");

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.add("Content-Type", "text/html; charset=utf-8");
    String message = "<script>";
    message += " alert('새 회원을 등록합니다.');";
    message += " location.href='/restapi/member/list2'; ";
    message += " </script>";
    return  new ResponseEntity(message, responseHeaders, HttpStatus.CREATED);
  }
}
