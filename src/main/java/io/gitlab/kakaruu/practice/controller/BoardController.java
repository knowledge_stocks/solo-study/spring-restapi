package io.gitlab.kakaruu.practice.controller;

import io.gitlab.kakaruu.practice.vo.ArticleVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/board")
public class BoardController {
  static Logger logger = LoggerFactory.getLogger(BoardController.class);

  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public ResponseEntity<List<ArticleVO>> list() {
    logger.info("GET /board/list");

    List<ArticleVO> list = new ArrayList<ArticleVO>();
    for (int i = 0; i < 10; i++) {
      ArticleVO vo = new ArticleVO();
      vo.setArticleNO(i);
      vo.setWriter("이순신"+i);
      vo.setTitle("안녕하세요"+i);
      vo.setContent("새 상품을 소개합니다."+i);
      list.add(vo);
    }

    return new ResponseEntity<List<ArticleVO>>(list, HttpStatus.OK);
  }

  @RequestMapping(value = "/{articleNO}", method = RequestMethod.GET)
  public ResponseEntity<ArticleVO> get (@PathVariable("articleNO") Integer articleNO) {
    logger.info("GET /board/{articleNO}");

    ArticleVO vo = new ArticleVO();
    vo.setArticleNO(articleNO);
    vo.setWriter("홍길동");
    vo.setTitle("안녕하세요");
    vo.setContent("홍길동 글입니다");

    return new ResponseEntity<ArticleVO>(vo,HttpStatus.OK);
  }

  @RequestMapping(value = "", method = RequestMethod.POST)
  public ResponseEntity<String> post (@RequestBody ArticleVO articleVO) {
    logger.info("POST /board");

    ResponseEntity<String>  resEntity = null;
    try {
      logger.info("addArticle 메서드 호출");
      logger.info(articleVO.toString());
      resEntity =new ResponseEntity<String>("ADD_SUCCEEDED",HttpStatus.OK);
    }catch(Exception e) {
      resEntity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
    }

    return resEntity;
  }

  //수정하기
  @RequestMapping(value = "/{articleNO}", method = RequestMethod.PUT)
  public ResponseEntity<String> put (@PathVariable("articleNO") Integer articleNO, @RequestBody ArticleVO articleVO) {
    logger.info("PUT /board/{id}");

    ResponseEntity<String>  resEntity = null;
    try {
      logger.info("modArticle 메서드 호출");
      logger.info(articleVO.toString());
      resEntity =new ResponseEntity<String>("MOD_SUCCEEDED",HttpStatus.OK);
    }catch(Exception e) {
      resEntity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
    }

    return resEntity;
  }

  //삭제하기
  @RequestMapping(value = "/{articleNO}", method = RequestMethod.DELETE)
  public ResponseEntity<String> delete (@PathVariable("articleNO") Integer articleNO) {
    logger.info("DELETE /board/{id}");

    ResponseEntity<String>  resEntity = null;
    try {
      logger.info("removeArticle 메서드 호출");
      logger.info(articleNO.toString());
      resEntity =new ResponseEntity<String>("REMOVE_SUCCEEDED",HttpStatus.OK);
    }catch(Exception e) {
      resEntity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
    }

    return resEntity;
  }
}
